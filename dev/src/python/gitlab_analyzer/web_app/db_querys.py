import re
import collections

from .utils import get_repo_collection


def get_commits_db(project_id: int):
    """Pesquisa dados sobre o {project_id} na base de dados dos repositorios"""

    mongo_collection = get_repo_collection(project_id)
    commits = mongo_collection.find_one({"feature_id": 3})["commits"]

    return commits


def get_directories_db(project_id: int, directory: str):
    """Retorna as diretorias e pastas da diretoria em questão, bem como as suas categorias"""

    mongo_collection = get_repo_collection(project_id)
    files = mongo_collection.find_one({"feature_id": 10})["files"]

    info_directories = []

    if directory == "root":
        for file in files:
            filename = file["name"].split("/")[0]
            if not any(
                directory["directory"] == filename for directory in info_directories
            ):
                info_directories.append(
                    {
                        "directory": filename,
                        "category": filename.split(".")[-1].upper(),
                        "churn": get_directory_churn(project_id, filename),
                    }
                )
    else:
        regexpr = re.compile(f"^{directory[5:]}/.*")
        dir_index = len(directory[5:].split("/"))
        for file in files:
            matches = re.search(regexpr, file["name"])
            if matches != None:
                print(file["name"])
                print(matches.group(0))
                filename = matches.group(0).split("/")[dir_index]
                if not any(
                    directory["directory"] == filename for directory in info_directories
                ):
                    info_directories.append(
                        {
                            "directory": filename,
                            "category": filename.split(".")[-1].upper(),
                            "churn": get_directory_churn(project_id, filename),
                        }
                    )

    return info_directories


def get_directory_churn(project_id: int, directory: str):
    """Devolve o Churn de uma diretoria"""

    mongo_collection = get_repo_collection(project_id)
    files = mongo_collection.find_one({"feature_id": 10})["files"]

    regexpr = re.compile(f"{directory}.*")
    file_count = 0
    total_churn = 0

    for file in files:
        matches = re.search(regexpr, file["name"])
        if matches != None:
            file_count += 1
            total_churn += file["churn"]

    total_churn /= file_count

    return total_churn


def get_files_by_category(project_id: int):
    """Retorna um dicionário de ficheiros por categoria"""

    mongo_collection = get_repo_collection(project_id)
    file_list = mongo_collection.find_one({"feature_id": 10})["files"]
    file_list.sort(key=lambda f: f["category"])
    files = {}
    for file in file_list:
        if file["category"] not in files:
            files[file["category"]] = [file["name"]]
        else:
            files[file["category"]].append(file["name"])

    return files


def get_files_by_author(project_id: int):
    """Devolve dicionario com ficheiros por autores"""

    mongo_collection = get_repo_collection(project_id)
    files = mongo_collection.find_one({"feature_id": 10})["files"]

    authors_dict = {}

    for file in files:
        if file["author"] not in authors_dict.keys():
            authors_dict[file["author"]] = [file["name"].split("/")[-1]]
        else:
            authors_dict[file["author"]].append(file["name"].split("/")[-1])

    return authors_dict


def get_files_by_churn(project_id: int):
    """Devolve dicionario com ficheiros por churn"""

    mongo_collection = get_repo_collection(project_id)
    files = mongo_collection.find_one({"feature_id": 10})["files"]

    churn_dict = {}
    churn: str = ""

    for file in files:
        churn = "1" if file["churn"] < 0.33 else ("2" if file["churn"] < 0.66 else "3")
        if churn not in churn_dict.keys():
            churn_dict[churn] = [file["name"].split("/")[-1]]
        else:
            churn_dict[churn].append(file["name"].split("/")[-1])

    return collections.OrderedDict(sorted(churn_dict.items()))


def get_files_name(project_id: int):
    """Devolva uma lista com todas as diretorias de ficheiros existentes"""
    mongo_collection = get_repo_collection(project_id)
    file_list = mongo_collection.find_one({"feature_id": 10})["files"]

    files = []
    for file in file_list:
        files.append(file["name"])

    return files


def get_dict_with_key(files: list, key: str) -> list:
    """Devolve uma lista de ficheiros, que é o valor da chave {key}, num dicionário presente em {files}.
    Caso esse dicionário não exista, é adicionado"""
    for i in range(len(files)):
        if type(files[i]) == dict:
            if key in files[i]:
                return files[i]

    files.append({key: []})
    return get_dict_with_key(files, key)


def create_jsn_tree(project_id: int) -> dict:
    """Cria um json de json's que representa os diretorios e ficheiros do repositorio"""
    paths = get_files_name(project_id)

    tree = {"root": []}

    for path in paths:
        splited = path.split("/")
        if len(splited) > 1:
            aux = get_dict_with_key(tree["root"], splited[0])
            aux = aux[splited[0]]
            for elem in splited[1:-1]:
                aux = get_dict_with_key(aux, elem)
                aux = aux[elem]
            aux.append(splited[-1])
        else:
            tree["root"].append(splited[-1])

    return tree
