from django.urls import path
from . import views

urlpatterns = [
    path("", views.homepage, name="homepage"),
    # path("register/", views.register, name="register"),
    # path("project/refresh", views.refresh_db, name = "refresh"),
    path(
        r"project/<int:projeto_id>/-/<path:directory>",
        views.dashboard_view,
        name="dash",
    ),
    path(r"project/<int:projeto_id>/tree", views.tree_view, name="tree"),
    path(r"project/<int:projeto_id>/group/autor/<str:author>", views.group_view_autor),
    path(
        r"project/<int:projeto_id>/group/churn/<str:churn_intensity>",
        views.group_view_churn,
    ),
    path(
        r"project/<int:projeto_id>/group/category/<path:category>", views.group_category
    ),
    path(r"button", views.back_click, name="previous_page"),
    path(r"project/<int:projeto_id>/files/<path:filename>", views.file_view),
]
