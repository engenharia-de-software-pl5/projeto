import requests
import os

from django.shortcuts import render, redirect
from django.http.response import HttpResponseRedirect
from django.http import HttpResponse
from dotenv import load_dotenv
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from datetime import datetime

from .structures import BackStack
from .get_data import get_all_info_homepage_api, get_files_content
from .db_querys import *
from .utils import is_file_or_directory

load_dotenv()

TOKEN = os.getenv("GITLAB_API_TOKEN")
connect_string = os.getenv("DATABASE_URL")
URL = "https://gitlab.com/api/v4/projects/"

back = BackStack()


def homepage(request) -> HttpResponse:
    """Devolve a view da Homepage"""

    if request.method == "POST":
        if request.POST["projeto_id"].isnumeric():
            return HttpResponseRedirect(
                "/project/" + request.POST["projeto_id"] + "/-/root"
            )

        return render(
            request,
            "web_app/index.html",
            {"error": "Bad Input, introduza apenas números!"},
        )
    elif request.method == "GET":

        back.push("/")

        return render(request, "web_app/index.html")

    return HttpResponse(status=405)


def dashboard_view(request, projeto_id, directory) -> HttpResponse:
    """Devolve a view da dashboard"""

    if request.method == "POST":
        if request.POST.get("projeto_id"):
            if request.POST["projeto_id"].isnumeric():
                return HttpResponseRedirect(
                    "/project/" + request.POST["projeto_id"] + "/-/root"
                )
            else:
                return render(
                    request,
                    "web_app/index.html",
                    {"error": "Bad Input, introduza apenas números!"},
                )
        elif request.POST.get("directory"):
            if (
                is_file_or_directory(
                    projeto_id, directory + "/" + request.POST["directory"]
                )
                == "directory"
            ):
                return HttpResponseRedirect(
                    "/project/"
                    + str(projeto_id)
                    + "/-/"
                    + directory
                    + "/"
                    + request.POST["directory"]
                )
            else:
                return HttpResponseRedirect(
                    "/project/"
                    + str(projeto_id)
                    + "/files/"
                    + directory
                    + "/"
                    + request.POST["directory"]
                )
        elif request.POST.get("agrupar"):
            if request.POST["agrupar"] == "autor":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/autor/all"
                )
            elif request.POST["agrupar"] == "churn":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/churn/all"
                )
            elif request.POST["agrupar"] == "categoria":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/category/all"
                )

    elif request.method == "GET":
        try:
            if directory == "root":
                get_all_info_homepage_api(projeto_id)
            date = datetime.utcnow()

            timeline_commits = get_commits_db(projeto_id)

            directories = get_directories_db(projeto_id, directory)

            get_files_by_churn(projeto_id)

            back.push(f"/project/{projeto_id}/-/{directory}")

            return render(
                request,
                "web_app/index_dashboard.html",
                {
                    "timeline_commits": timeline_commits,
                    "date": date,
                    "back": back,
                    "directories": directories,
                    "projeto_id": str(projeto_id),
                },
            )
        except requests.exceptions.HTTPError as err:
            erro = str(err)
            if erro[:3] == "404":
                return render(
                    request, "web_app/index.html", {"error": "Projeto não encontrado!"}
                )
            return HttpResponse(err)
    else:
        return HttpResponse(status=405)


def group_view_autor(request, projeto_id, author):

    if request.method == "POST":
        if request.POST.get("dashboard"):
            return HttpResponseRedirect("/project/" + str(projeto_id) + "/-/root")
        elif request.POST.get("projeto_id"):
            if request.POST["projeto_id"].isnumeric():
                return HttpResponseRedirect(
                    "/project/" + request.POST["projeto_id"] + "/-/root"
                )
            else:
                return render(
                    request,
                    "web_app/index.html",
                    {"error": "Bad Input, introduza apenas números!"},
                )
        elif request.POST.get("author"):
            return HttpResponseRedirect(
                "/project/" + str(projeto_id) + "/group/autor/" + request.POST["author"]
            )
        elif request.POST.get("filter"):
            if request.POST["filter"] == "autor":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/autor/all"
                )
            if request.POST["filter"] == "churn":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/churn/all"
                )
            if request.POST["filter"] == "category":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/category/all"
                )

    elif request.method == "GET":
        try:
            files_authores = get_files_by_author(projeto_id)

            date = datetime.utcnow()

            back.push(f"/project/{projeto_id}/group/autor/all")

            if author == "all":
                return render(
                    request,
                    "web_app/groupby_author_view.html",
                    {
                        "date": date,
                        "back": back,
                        "files": files_authores,
                        "author": "",
                    },
                )
            else:
                return render(
                    request,
                    "web_app/groupby_author_view.html",
                    {
                        "date": date,
                        "back": back,
                        "files": files_authores[author],
                        "author": author,
                    },
                )

        except requests.exceptions.HTTPError as err:
            erro = str(err)
            if erro[:3] == "404":
                return render(
                    request, "web_app/index.html", {"error": "Projeto não encontrado!"}
                )
            return HttpResponse(err)

    else:
        return HttpResponse(status=405)


def group_view_churn(request, projeto_id, churn_intensity):

    if request.method == "POST":
        if request.POST.get("dashboard"):
            return HttpResponseRedirect("/project/" + str(projeto_id) + "/-/root")
        elif request.POST.get("projeto_id"):
            if request.POST["projeto_id"].isnumeric():
                return HttpResponseRedirect(
                    "/project/" + request.POST["projeto_id"] + "/-/root"
                )
            else:
                return render(
                    request,
                    "web_app/index.html",
                    {"error": "Bad Input, introduza apenas números!"},
                )
        elif request.POST.get("churn"):
            return HttpResponseRedirect(
                "/project/" + str(projeto_id) + "/group/churn/" + request.POST["churn"]
            )
        elif request.POST.get("filter"):
            if request.POST["filter"] == "autor":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/autor/all"
                )
            if request.POST["filter"] == "churn":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/churn/all"
                )
            if request.POST["filter"] == "category":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/category/all"
                )

    elif request.method == "GET":
        try:
            files_churn = get_files_by_churn(projeto_id)

            date = datetime.utcnow()

            back.push(f"/project/{projeto_id}/group/churn/all")

            if churn_intensity == "all":
                return render(
                    request,
                    "web_app/groupby_churn_view.html",
                    {
                        "date": date,
                        "back": back,
                        "files": files_churn,
                        "churn": "",
                    },
                )
            else:
                return render(
                    request,
                    "web_app/groupby_churn_view.html",
                    {
                        "date": date,
                        "back": back,
                        "files": files_churn[churn_intensity],
                        "churn": churn_intensity,
                    },
                )

        except requests.exceptions.HTTPError as err:
            erro = str(err)
            if erro[:3] == "404":
                return render(
                    request, "web_app/index.html", {"error": "Projeto não encontrado!"}
                )
            return HttpResponse(err)

    else:
        return HttpResponse(status=405)


def group_category(request, projeto_id, category):

    if request.method == "POST":
        if request.POST.get("dashboard"):
            return HttpResponseRedirect("/project/" + str(projeto_id) + "/-/root")
        elif request.POST.get("projeto_id"):
            if request.POST["projeto_id"].isnumeric():
                return HttpResponseRedirect(
                    "/project/" + request.POST["projeto_id"] + "/-/root"
                )
            else:
                return render(
                    request,
                    "web_app/index.html",
                    {"error": "Bad Input, introduza apenas números!"},
                )
        elif request.POST.get("category"):
            return HttpResponseRedirect(
                "/project/"
                + str(projeto_id)
                + "/group/category/"
                + request.POST["category"]
            )
        elif request.POST.get("filter"):
            if request.POST["filter"] == "category":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/category/all"
                )

            elif request.POST["filter"] == "autor":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/autor/all"
                )

            elif request.POST["filter"] == "churn":
                return HttpResponseRedirect(
                    "/project/" + str(projeto_id) + "/group/churn/all"
                )

    elif request.method == "GET":
        try:
            files_category = get_files_by_category(projeto_id)

            date = datetime.utcnow()

            back.push(f"/project/{projeto_id}/group/category/all")

            if category == "all":
                return render(
                    request,
                    "web_app/group_category.html",
                    {
                        "date": date,
                        "back": back,
                        "files": files_category,
                        "category": "",
                    },
                )
            else:
                return render(
                    request,
                    "web_app/group_category.html",
                    {
                        "date": date,
                        "back": back,
                        "files": files_category[category],
                        "category": category,
                    },
                )

        except requests.exceptions.HTTPError as err:
            erro = str(err)
            if erro[:3] == "404":
                return render(
                    request, "web_app/index.html", {"error": "Projeto não encontrado!"}
                )
            return HttpResponse(err)

    else:
        return HttpResponse(status=405)


def back_click(request):
    """Redireciona para a vista anterior"""
    url = back.pop()

    with open("log.log", "a") as logger:
        logger.write(str(back))
    back.is_stack = True
    redirect_response = redirect(url, permanent=True)
    return redirect_response


def register(request):
    """Regista utilizador na aplicação"""
    if request.method == "POST":
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password1"]
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect("homepage")
    else:
        form = UserCreationForm()

    context = {"form": form}
    return render(request, "registration/register.html", context)


def tree_view(request, projeto_id):
    """ """
    tree = create_jsn_tree(projeto_id)

    timeline_commits = get_commits_db(projeto_id)
    directories = get_directories_db(projeto_id, "root")
    date = datetime.utcnow()

    back.push(f"/project/{projeto_id}/tree")

    return render(
        request,
        "web_app/index_dashboard_tree.html",
        {
            "timeline_commits": timeline_commits,
            "date": date,
            "back": back,
            "directories": directories,
            "projeto_id": str(projeto_id),
            "tree": tree,
        },
    )


def file_view(request, projeto_id, filename):

    if request.method == "POST":
        if request.POST.get("projeto_id"):
            if request.POST["projeto_id"].isnumeric():
                return HttpResponseRedirect(
                    "/project/" + request.POST["projeto_id"] + "/-/root"
                )
            else:
                return render(
                    request,
                    "web_app/index.html",
                    {"error": "Bad Input, introduza apenas números!"},
                )

    elif request.method == "GET":

        file_content = get_files_content(filename[5:])
        date = datetime.utcnow()

        back.push(f"project/{projeto_id}/files/{filename}")

        return render(
            request,
            "web_app/file_view.html",
            {
                "file_name": filename.split("/")[-1],
                "date": date,
                "back": back,
                "file_content": file_content,
            },
        )
