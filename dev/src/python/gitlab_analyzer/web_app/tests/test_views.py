from django.urls import reverse
from django.contrib.auth import get_user_model


import pytest


''' @pytest.mark.parametrize("param", [("homepage"), ("register"), ("project_not_found")])
def test_render_views(client, param):
    """Função para testar os endpoints que são vistas"""
    
    temp_url = reverse(param)
    response = client.get(temp_url)
    assert response.status_code == 200 '''

""" def test_quick(client):
    response = client.get(reverse("homepage"))
    print(response.content)
    assert 1 == 1 """


''' @pytest.mark.django_db
def test_user_register(client, user_data):
    """Função para testar a criação de um utilizador"""
    user_model = get_user_model()
    assert user_model.objects.count() == 0  # verificar que não existe
    register_url = reverse("register")
    response = client.post(register_url, user_data)
    assert user_model.objects.count() == 1  # verificar que o user foi criado
    assert response.status_code == 302  # verificar que foi feito um redirect '''
