import pytest
from django.contrib.auth import get_user_model


@pytest.fixture
def user_data():
    return {
        "username": "User_name",
        "password1": "user_password543",
        "password2": "user_password543",
    }


@pytest.fixture
def empty_form():
    return {}


@pytest.fixture
def valid_project_id():
    return {"project_id": "29777940"}


''' @pytest.fixture(scope = "session")
def logged_user(db):
    user_model = get_user_model()
    assert user_model.objects.count() == 0  # verificar que não existe
    return 

def test_user_register(client, user_data):
    """Função para testar a criação de um utilizador"""
    user_model = get_user_model()
    assert user_model.objects.count() == 0  # verificar que não existe
    register_url = reverse("register")
    response = client.post(register_url, user_data)
    assert user_model.objects.count() == 1  # verificar que o user foi criado
    assert response.status_code == 302  # verificar que foi feito um redirect '''
