from django.template import Library
from datetime import datetime
import json

register = Library()


@register.filter(name="parse_date")
def parse_date(date, format) -> str:
    """Converte a data {date} que vem num formato estático do Gitlab, para o {format}"""
    aux: datetime = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S.%f%z")
    return datetime.strftime(aux, format)


@register.filter(name="js_datetime")
def js_datetime(date) -> str:
    """Converte o tempo de segundos desde o epoch para milissegundos desde o epoch"""
    return str(datetime.timestamp(date) * 1000)


@register.filter(name="check_button")
def check_button(stack) -> bool:
    """Verifica se a pilha das vistas tem pelo menos 1 elemento"""
    return "true" if len(stack) > 0 else "false"


@register.filter(name="concat")
def concat(str_1: str, str_2: str) -> str:
    return str_1 + str_2


@register.filter(name="is_folder")
def is_folder(file) -> bool:
    return type(file) == dict


@register.filter(name="to_json")
def to_json(dictionary: dict) -> str:
    return json.dumps(dictionary)


@register.filter(name="unpack_dict")
def unpack_dict(directory: dict) -> list:
    keys = list(directory.keys())
    return keys[0], directory[keys[0]]


@register.filter(name="file_html")
def file_html(file: str) -> str:
    html = f"<li><span><i class='fa fa-file'></i>&nbsp;{file}</span></li>"
    return html


@register.filter(name="dir_elems_htlm")
def dir_elems_html(files: list) -> str:
    html = ""

    for file in files:
        if type(file) == dict:
            html += dir_html(file)
        else:
            html += file_html(file)
    return html


@register.filter(name="dir_html")
def dir_html(directory: dict) -> str:
    key, files = unpack_dict(directory)

    html = f"<li><span><i class='fa fa-folder'></i>&nbsp;{key}</span>"
    if len(files) > 0:
        html += "<ul>"
        html += dir_elems_html(files)
        html += "</ul>"
    html += "</li>"

    return html


@register.filter(name="tree_html")
def tree_html(tree_json: dict) -> str:
    html = ""
    files = tree_json["root"]  # unpack_dict(tree_json, result)
    html += dir_elems_html(files)
    return html
