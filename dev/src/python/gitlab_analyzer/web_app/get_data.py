import requests
import re
import os
import subprocess

from os import getenv
from dotenv import load_dotenv
from functools import reduce
from pydriller import Repository

from .utils import get_repo_collection


load_dotenv()
TOKEN = getenv("GITLAB_API_TOKEN")
URL: str = "https://gitlab.com/api/v4/projects/"


def get_all_info_homepage_api(project_id: int) -> None:
    """Guarda toda a informação necessária à homepage da dashboard"""
    get_basic_info_api(project_id)
    get_files_pydriller(project_id)
    # get_commits_api(project_id)
    # get_directories_api(project_id)
    # get_authors_commits_pydriller(project_id)


def get_basic_info_api(project_id: int) -> None:
    """Guarda na base de dados informacao basica sobre um repositorio"""

    r1: requests.request = requests.get(
        url=URL + str(project_id),
        headers={"PRIVATE-TOKEN": TOKEN},
    )

    r1.raise_for_status()

    json_dict = r1.json()
    json_dict["feature_id"] = 1
    collection = get_repo_collection(project_id)
    if collection.find_one({"feature_id": 1}):
        collection.delete_one({"feature_id": 1})
        collection.insert_one(json_dict)
    else:
        collection.insert_one(json_dict)


def get_churn_authors_commits_pydriller(project_id: int):
    """Retorna os autores dos ficheiros do repositorio"""

    files_authores: dict(str, str) = {}
    files_churns = {}
    dict_files_commit = {}
    dict_commits = {"feature_id": 3, "commits": []}

    for commit in Repository("/gitlab_analyzer/temp_repo").traverse_commits():

        commit_info = {
            "title": commit.msg.split("\n")[0],
            "committer_name": commit.committer.name,
            "committer_date": commit.committer_date.strftime("%d/%m/%Y, %H:%M:%S"),
        }
        dict_commits["commits"].append(commit_info)

        files = list(map(lambda file: file.filename, commit.modified_files))
        author = commit.author.name
        for file in files:
            if file not in files_authores:
                files_authores[file] = author

        for file in commit.modified_files:

            if file.new_path not in dict_files_commit:
                dict_files_commit[file.new_path] = [commit_info]
            else:
                dict_files_commit[file.new_path].append(commit_info)

            files_churns[file.new_path] = (
                files_churns.get(file.new_path, 0)
                + file.added_lines
                + file.deleted_lines
            )

    max_churn: int = reduce(lambda x, y: max(x, y), files_churns.values())
    normalized_churns: dict[str, float] = dict(
        map(lambda x, y: (x, y / max_churn), files_churns.keys(), files_churns.values())
    )

    mongo_collection = get_repo_collection(project_id)
    mongo_collection.find_one_and_delete({"feature_id": 3})
    dict_commits["commits"].reverse()
    mongo_collection.insert_one(dict_commits)

    return files_authores, normalized_churns, dict_files_commit


def get_files_pydriller(project_id: int):

    collection = get_repo_collection(project_id)
    web_url = collection.find_one({"feature_id": 1})["web_url"]
    os.system("rm -rf /gitlab_analyzer/temp_repo")
    os.system("mkdir /gitlab_analyzer/temp_repo")
    os.system("git clone " + web_url + " /gitlab_analyzer/temp_repo")

    json_dict = {"feature_id": 10, "files": []}
    authors, churn, files_commit = get_churn_authors_commits_pydriller(project_id)

    files = (
        subprocess.run(
            "cd temp_repo; git ls-tree --full-tree -r  HEAD",
            capture_output=True,
            shell=True,
        )
        .stdout.decode("utf-8")
        .split("\n")
    )
    logger = open("log.log", "w")
    logger.write(str(files))
    logger.close()
    # logger = open("log.log", "a")
    for file in files:
        if file != "":
            file_name = re.split("\t", file)[-1]
            json_dict["files"].append(
                {
                    "name": file_name,
                    "author": authors[file_name.split("/")[-1]],
                    "churn": churn[file_name],
                    "category": file_name.split(".")[-1].upper(),
                    "commits": files_commit[file_name],
                }
            )

    mongo_collection = get_repo_collection(project_id)
    mongo_collection.find_one_and_delete({"feature_id": 10})
    mongo_collection.insert_one(json_dict)


def get_files_content(file_path: str) -> str:
    """Get file content from database given a path"""

    try:
        file = open("temp_repo/" + file_path, "r")
    except FileNotFoundError:
        return "File Not Found"

    content = file.read()
    file.close()

    return content
