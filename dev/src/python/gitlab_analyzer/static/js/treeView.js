$(function () {
  $("#tree li:has(ul)")
    .addClass("parent_li")
    .find(" > span")
    .attr("title", "Collapse this branch");
  $("#tree li.parent_li > span").on("click", function (e) {
    var children = $(this).parent("li.parent_li").find(" > ul > li");
    if (children.is(":visible")) {
      children.hide("fast");
      $(this)
        .attr("title", "Expand this branch")
        .find(" > i")
        .removeClass("fa fa-folder-open")
        .addClass("fa fa-folder");
    } else {
      children.show("fast");
      $(this)
        .attr("title", "Collapse this branch")
        .find(" > i")
        .addClass("fa fa-folder-open");
    }
    e.stopPropagation();
  });
});
$(".hideOnLoad").hide("fast");
