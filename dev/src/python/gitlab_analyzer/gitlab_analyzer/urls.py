from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("admin/", admin.site.urls, name="admin"),
    path("", include("web_app.urls")),
    path("accounts/", include("django.contrib.auth.urls")),
]
