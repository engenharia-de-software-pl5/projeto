django>=3.2.8
pymongo[srv]>=3.12.1
requests
python-dotenv
pydriller
gunicorn
