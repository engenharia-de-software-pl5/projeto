from pydriller import Repository
from datetime import datetime

repo = "https://gitlab.com/engenharia-de-software-pl5/projeto/"

# aaaa/mm/dd
date1 = datetime(2021, 10, 25)
# date2 = datetime(2021, 10, 31)
date2 = datetime.now()

total_commits = 0
commits = {}


for commit in Repository(
    repo, since=date1, to=date2, include_remotes=True
).traverse_commits():
    total_commits += 1

    print(commit.author.name)
    print(commit.msg)
    print(commit.committer_date)
    # print('Branches -> ', commit.branches)

    if commit.author.name not in commits:
        commits[commit.author.name] = 1
    else:
        commits[commit.author.name] += 1

    # print('Modified files')
    # for file in commit.modified_files:
    #     print(file.filename)
    #     print('File complexity:', file.complexity)
    #     # print(file.source_code)

    print("--------")

print(f"Total commits -> {total_commits}")
print(commits)
