import gitlab
import os
import json
import re
from dotenv import load_dotenv


def verificarDefeitosBootstrap(issues):
    print("---------ISSUES DE DEFEITOS DE BOOTSTRAP---------")
    for issue in issues:
        if bool(re.match("def-design-bootstrap [0-9.]*", issue.title)):
            print("def-design-bootstrap ", issue.iid)
            print("     Title: ✔️")
        else:
            print("     Title: ❌")
        if ("**Tempo estimado para desenvolvimento da especificação:**" in issue.attributes["description"]):
            print("     Tempo estimado para desenvolvimento da especificação: ✔️")
        else:
            print("     Tempo estimado para desenvolvimento da especificação: ❌")


def get_project(project_id):
    # Criar ficheiro .env com o token de acesso à API do gitlab
    load_dotenv()

    # get repository and authenticate
    gl = gitlab.Gitlab("https://gitlab.com/", private_token=os.getenv("GITLAB_APIKEY"))
    gl.auth()

    # get project
    project = gl.projects.get(project_id)

    # get issues
    return project.issues.list(sort="asc", all=True, scope="all")


if __name__ == "__main__":
    issues = get_project(29777940)
    verificarDefeitosBootstrap(issues)
