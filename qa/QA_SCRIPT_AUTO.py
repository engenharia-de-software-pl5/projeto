import gitlab
import os
import json
import re
from dotenv import load_dotenv


def verificarRequisito(issues):
    print("---------REQUISITOS---------")
    for issue in issues:
        if bool(re.match("Requisito [0-9.]*", issue.title)):
            print("issue: ", issue.iid)
            if bool(
                re.match("\**User Story [0-9.]*:\**", issue.attributes["description"])
            ):
                print("     User Story: ✔️")
            else:
                print("     User Story: ❌")
            if "**Requisito:**" in issue.attributes["description"]:
                print("     Requisito: ✔️")
            else:
                print("     Requisito: ❌")
            if "**Ordem de eventos desejada:**" in issue.attributes["description"]:
                print("     Ordem de eventos desejada: ✔️")
            else:
                print("     Ordem de eventos desejada: ❌")
            if (
                "**Tempo estimado para desenvolvimento da especificação:**"
                in issue.attributes["description"]
            ):
                print("     Tempo estimado para desenvolvimento da especificação: ✔️")
            else:
                print("     Tempo estimado para desenvolvimento da especificação: ❌")


def verificarAta(issues):
    print("---------ATAS---------")
    for issue in issues:
        if "Ata Reunião" in issue.title:
            print("issue: ", issue.iid)
            if bool(
                re.match(
                    "Ata Reunião [0-9]+ - [0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9] - .* - (Remoto|Presencial)",
                    issue.title,
                )
            ):
                print("     Titulo: ✔️")
            else:
                print("     Titulo: ❌")
            if "Atas" in issue.labels:
                print("     Label: ✔️")
            else:
                print("     Label: ❌")


def verificarBackend(issues):
    print("---------ISSUES DE DEFEITOS DE BACKEND---------")
    for issue in issues:
        if bool(re.match("def-backend-mock (Requisito [0-9.]*)", issue.title)):
            print("description:")
            if bool(re.match("\**Código:*\**", issue.attributes["description"])):
                print("     Código: ✔️")
            else:
                print("     Código: ❌")
            if "**Especificação do problema:**" in issue.attributes["description"]:
                print("     Especificação do problema: ✔️")
            else:
                print("     Especificação do problema: ❌")
            if "**Alteração sugerida:**" in issue.attributes["description"]:
                print("     Alteração sugerida: ✔️")
            else:
                print("     Alteração sugerida: ❌")


def verificarDefeitosBootstrap(issues):
    print("---------ISSUES DE DEFEITOS DE BOOTSTRAP---------")
    for issue in issues:
        if bool(re.match("def-design-bootstrap [0-9.]*", issue.title)):
            if (
                "**Tempo estimado para desenvolvimento da especificação:**"
                in issue.attributes["description"]
            ):
                print("     Tempo estimado para desenvolvimento da especificação: ✔️")
            else:
                print("     Tempo estimado para desenvolvimento da especificação: ❌")


def get_project(project_id):
    # Criar ficheiro .env com o token de acesso à API do gitlab
    load_dotenv()

    # get repository and authenticate
    gl = gitlab.Gitlab("https://gitlab.com/", private_token=os.getenv("GITLAB_APIKEY"))
    gl.auth()

    # get project
    project = gl.projects.get(project_id)

    # get issues
    return project.issues.list(sort="asc", all=True, scope="all")


if __name__ == "__main__":
    issues = get_project(29777940)
    verificarRequisito(issues)
    verificarAta(issues)
    verificarBackend(issues)
    verificarDefeitosBootstrap(issues)
