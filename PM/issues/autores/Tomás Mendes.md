#293 -> Alteração do perfil <br>
#272 -> def-backend suporte multi-user <br>
#269 -> Ata Aula 9 - 30/11/2021 - Aula Presencial <br>
#263 -> Fix para usar o ID do nosso repositório <br>
#257 -> def-issue-format Requisito 10.1 <br>
#241 -> Testes Funcionais 2 <br>
#240 -> Merge staging ->main e Deploy da aplicação <br>
#239 -> Acrescentar os requisitos desenvolvidos ao testes funcionais <br>
#237 -> Criação de um ficheiro de versões <br>
#236 -> [Testes] Requisito 4.2.3 <br>
#235 -> [Testes] Requisito 4.2.4 <br>
#233 -> [Testes] Requisito 4.2.2.3 <br>
#232 -> [Testes] Requisito 4.2.1 <br>
#231 -> [Testes] Requisito 4.2.5 <br>
#230 -> [Testes] Requisito 4.2.2.1 <br>
#229 -> [Testes] Requisito 4.2.2 <br>
#226 -> [Staging] 22/11 - 28/11 <br>
#222 -> [Defeito][Frontend] Tabela de Autores <br>
#221 -> [Defeito][Backend] Requisito 4.2.1 <br>
#220 -> Matriz de Rastreabilidade <br>
#219 -> Testes Funcionais 1 <br>
#209 -> [Integração]  Requisito 4.2.1 <br>
#200 -> [Docker] Adicionar chave ssh para usar pydriller <br>
#197 -> [Chores] Alterações aos README <br>
#196 -> [QA] Verificação e correção de erros de análise estática <br>
#195 -> [TESTES] Requisito 4.2.1 <br>
#194 -> [TESTES] Alteração da convenção <br>
#189 -> Ata Gestores 1 - 22/11/2021 - Apresentação do trabalho feito pelas equipas - Remoto <br>
#188 -> Ata Aula 8 - 23/11/2021 - Aula Presencial <br>
#177 -> [Design] Problemas formatação Tabelas <br>
#176 -> [Testes] Requisito 4.1.7 <br>
#172 -> [Defeito Backend] Requisito 4.1.1 <br>
#171 -> Ata Testes 1 - 22/11/2021 - Vários assuntos da equipa de testes - Remoto <br>
#166 -> Deploy automatico <br>
#165 -> Aplicação Live <br>
#163 -> [Testes] Requisito 4.1.8 <br>
#162 -> [Testes] Requisito 4.1.6 <br>
#161 -> [Importante] Alterações da pipeline <br>
#157 -> [Testes] Requisito 4.1.5 <br>
#156 -> [CI/CD] Testar Pipeline <br>
#154 -> Ata Aula 7 - 16/11/2021 - Aula Presencial <br>
#152 -> Ata Reunião 9 - 15/11/2021 - Apresentação de trabalho pelos gestores - Remoto <br>
#149 -> Script Testes <br>
#144 -> Requisitos Semana 16/11 - 21/11 <br>
#136 -> [Defeito Bootstrap] Design requisito 4.1.1 Incompleto <br>
#133 -> Ficheiro para facilitar a atribuição de trabalho <br>
#132 -> [Testes] Requisito 4.1.4 - Por testar <br>
#131 -> [Testes] Requisito 4.1.3 <br>
#130 -> [Testes] Requisito 4.1.2 - Por testar <br>
#129 -> [BACKEND] Requisito 4.1.4 <br>
#128 -> [BACKEND] Requisito 4.1.3 <br>
#127 -> [BACKEND] Requisito 4.1.2 <br>
#125 -> Testes com o pytest <br>
#124 -> [Testes] Requisito 4.1.1 <br>
#113 -> [Design] Bootstrap: Requisito 4.2.2 <br>
#112 -> [Design] Bootstrap: Requisito 4.2.1 <br>
#107 -> [Design] Bootstrap: Requisito 4.1.4 (Requisito avançado) <br>
#105 -> [Design] Bootstrap: Requisito 4.1.2 (Requisito avançado) <br>
#100 -> Ata Reunião 7 - 11/11/2021 - Tarefas Equipa de Testes - Presencial <br>
#98 -> [Testes] Aplicação base - Passou <br>
#97 -> Convenção de issues relacionados com testes <br>
#94 -> Alterações á aplicação base <br>
#93 -> Ata Aula 6 - 9/11/2021 - Aula - Presencial <br>
#91 -> Requisito 4.2.2 <br>
#89 -> Ata Design 3 - 4/11/2021 - Desenho da interface - Remoto <br>
#88 -> Ata Aula 5 - 2/11/2021 - Aula Presencial <br>
#86 -> [Arch] Nivel de componentes da arquitetura <br>
#85 -> Requisito 4.3.2 <br>
#84 -> Deploy da aplicação <br>
#83 -> Aplicação base em docker [INSTALAR] <br>
#77 -> Ata Design 2 - 26/10/2021 - Decisões de design - Remoto <br>
#76 -> Ata Design 1 - 20/10/2021 - Reunião Design - Presencial <br>
#74 -> Ata Reunião 2 - 25/10/2021 - Reunião de Trabalho - Remoto <br>
#73 -> Testes com o Pydriller <br>
#72 -> Requisito 4.2.2.2 <br>
#71 -> Script para verificar formato dos requisitos <br>
#70 -> Ata Aula 4 - 19/10/2021 - Aula Presencial <br>
#68 -> Reunião - Equipa de design 20/10/2021 <br>
#67 -> Requisito 0 - TESTE <br>
#66 -> Revisão do método de desenvolvimento <br>
#61 -> Fazer um standart para as atas <br>
#59 -> Adicionar tecnologias usadas <br>
#58 -> Criação de uma ferramenta para tempo despendido <br>
#55 -> Requisito 4.2.5 <br>
#53 -> Inicio do projeto <br>
#52 -> Requisito 10.1 <br>
#51 -> Requisito 4.2.4 <br>
#50 -> Requisito 4.2.3 <br>
#49 -> Requisito 4.2.2.3 <br>
#48 -> Ata Reunião 1 - 15/10/2021 - Atribuição de cargos Presencial <br>
#47 -> Requisito 4.6.3 <br>
#46 -> Requisito 4.2.2.1 <br>
#45 -> Requisito 4.1.6 <br>
#44 -> Requisito 4.2.1 <br>
#43 -> Requisito 4.1.8 <br>
#42 -> Requisito 4.1.5 <br>
#41 -> Requisito 4.1.7 <br>
#40 -> Requisito 4.1.4 (Requisito Avançado) <br>
#39 -> Requisito 4.1.3 <br>
#38 -> Máximo de Requisitios: 10 <br>
#37 -> Máximo de Reviews: 2 <br>
#36 -> Requisito 4.1.2 (Requisito Avançado) <br>
#35 -> Requisito 4.1.1 <br>
#33 -> Requisitos <br>
#32 -> Users Storys <br>
#31 -> [Requisito] Teste <br>
#30 -> Ata Aula 2 - 12/10/2021 - Aula Presencial <br>
#29 -> Kanban Board <br>
#26 -> Users Stories <br>
#24 -> Requerimentos <br>
#20 -> Tutorial Django e MongoDB <br>
#17 -> [API Gitlab] Pesquisa sobre o token de acesso à API do gitlab <br>
#16 -> [BD] Pesquisa sobre tecnologias de base de dados. <br>
#15 -> Ata Aula 1 - 28/09/2021 - Aula Presencial <br>
#12 -> Semana #1 <br>
#1 -> Test <br>
