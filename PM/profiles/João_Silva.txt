nome: João Cordeiro Veloso da Silva
nº estudante: 2019217672
email: uc2019217672@student.uc.pt
cargos: Developer


Tempo dispendido: 45h10min
Issues:

https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/291
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/290
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/250
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/247
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/227
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/222
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/215
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/177
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/167
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/159
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/151
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/143
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/142
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/136
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/135
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/114
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/108
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/106
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/104
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/96
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/63
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/60
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/41
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/21
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/2
https://gitlab.com/engenharia-de-software-pl5/projeto/-/merge_requests/166
https://gitlab.com/engenharia-de-software-pl5/projeto/-/merge_requests/150
https://gitlab.com/engenharia-de-software-pl5/projeto/-/merge_requests/145
https://gitlab.com/engenharia-de-software-pl5/projeto/-/merge_requests/141
