Nome: Simão Carvalho Monteiro
Nº Estudante: 2019215412
Email: uc2019215412@student.uc.pt
Cargos: Developer, Design

Tempo dispendido: 34h30m

Issues:
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/295
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/282
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/254
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/224
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/218
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/216
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/215
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/182
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/111
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/49
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/19
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/9
