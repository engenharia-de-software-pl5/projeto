# DeiWare
## Projeto de Engenharia de Software PL5
## FCTUC - DEI - 2021/2022

### Objetivo
O projeto consiste em criar um website que servia de dashboard para analisar repositórios de GitLab, de acordo com os vários critérios e processos aprendidos na cadeira de Engenharia de Software.

### Atas
Existe um ficheiro em [pm/atas/README.md](https://gitlab.com/engenharia-de-software-pl5/projeto/-/blob/main/PM/atas/README.md) onde se encontram todas as atas de todas as reuniões feitas

### Aplicação
A aplicação está a correr numa máquina virtual fornecida pelo DEI neste [endereço](http://10.17.0.237:8080/). É necessário estar connectado á rede do DEI, para tal, é possível usar um VPN para poder usar esta rede remotamente. As instruções estão no site do [helpdesk](https://helpdesk.dei.uc.pt/configuration-instructions/vpn-access/).

### Membros:
- Tomás Mendes -> Gestor de Repositório, Líder de Testes, CI/CD, Deployments
- Alexandre Andrade -> Gestor de Projeto, Líder de Desenvolvimento, Líder de Produto, Developer
- Davide Areias -> Líder de Controlo de Qualidade, Design
- Joel Oliveira -> Líder de Arquitetura, Developer, CI/CD
- André Graça -> Tester
- Gonçalo Ferreira -> Design
- João Dionísio -> Controlo de Qualidade, Design
- João Silva -> Gestor Cliente, Líder de Design
- ~~José Braz -> Developer~~
- Marco Pais -> Developer
- Noémia Gonçalves -> Design
- ~~Rafael Silva -> Developer~~
- ~~Ricardo Simões -> Developer~~
- Rui Costa -> Tester
- ~~Rui Moniz -> Developer~~
- José Silva ->  Controlo de Qualidade
- Simão Monteiro -> Developer, Design
- ~~Telmo Correia -> Developer~~
- Paulo Meira -> Tester

### Tecnologias usadas
- Python
    - [Django](https://gitlab.com/engenharia-de-software-pl5/projeto/-/blob/main/env/django.md)
    - [Pymongo](https://pypi.org/project/pymongo/) (modulo para fazer interação entre Django e MongoDB)
- [MongoDB](https://gitlab.com/engenharia-de-software-pl5/projeto/-/blob/main/env/mongodb.md) (base de dados NoSQL)
- Bootstrap
    - [Bootstrap Studio](https://gitlab.com/engenharia-de-software-pl5/projeto/-/blob/main/env/bootstrap_studio.md) (ferramenta para desenvolvimento de frontend)
- [Structurizr](https://gitlab.com/engenharia-de-software-pl5/projeto/-/blob/main/env/structurizr.md) (ferramenta usada para fazer arquitetura)

