# Processes

## Ficheiros relacionados com os processos envolvidos no desenvolvimento do projeto

### Commits e Merge requests

**0 - Pull.** Devem dar sempre pull e garantirem que têm a versão mais atualizada do branch que pretendem copiar, para evitar conflitos. <br>
**1 - Criar um branch.** Podem criar um branch de várias formas, no site, através de uma interface para o git ou por um issue. O Erro que muitos têm feito é tentar dar commit no main. Como não têm permissões, o git cria um branch e merge request para tal. Evitem fazer isso ao máximo. <br>
**2 - Commit.** Selecionar o branch criado e fazer todos os commits nesse branch. Podem fazer quantos commits quiserem nesse branch. <br>
**3 - Merge request.** Quando quiserem que as vossas mudanças fiquem no repositório fazem um merge request, sendo o source o branch criado e o target o main ou outro branch. Estejam atentos ou tenham as notificações ativas porque podem ser feitos comentários nos merge requests para fazerem algum alteração. Mais tarde será necessário que as coisas sejam testadas e aprovadas por várias pessoas. <br>
