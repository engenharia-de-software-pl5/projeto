# Organização da base de dados mongodb

## Estrutura
- Base de dados mongodb única para guardar a informação necessária para a realização dos requesitos, com o nome 'repdb';
- Uma coleção por repositório, o nome da coleção será o id do repositório em questão;
- A informação de cada requisito corresponderá a um documento com um id fixo igual em todos as coleções.

### Id's documentos
- Requisito 4.1.1 - feature_id 1
- Requisito 4.1.2 - feature_id 2
- Requisito 4.1.3 - feature_id 3
- Requisito 4.1.4 - feature_id 4
- Requisito 4.1.5 - feature_id 5
- Requisito 4.1.6 - feature_id 6
- Requisito 4.1.7 - feature_id 7
- Requisito 4.1.8 - feature_id 8
- Requisito 4.2.1 - feature_id 9
- Requisito 4.2.2 - feature_id 10
- Requisito 4.2.2.1 - feature_id 11
- Requisito 4.2.2.2 - feature_id 12
- Requisito 4.2.2.3 - feature_id 13
- Requisito 4.2.3 - feature_id 14
- Requisito 4.2.4 - feature_id 15
- Requisito 4.2.5 - feature_id 16
- Requisito 4.3.1.1 - feature_id 17
- Requisito 4.3.1.2 - feature_id 18
- Requisito 4.3.1.3 - feature_id 19
- Requisito 4.3.2.1 - feature_id 20
- Requisito 4.3.3.1 - feature_id 21
- Requisito 4.4.1.1 - feature_id 22
- Requisito 4.4.1.2 - feature_id 23
- Requisito 4.4.1.3 - feature_id 24
- Requisito 4.4.1.4 - feature_id 25
- Requisito 4.4.2.1 - feature_id 26
- Requisito 4.4.2.2 - feature_id 27
- Requisito 4.4.2.3 - feature_id 28
- Requisito 4.4.2.4 - feature_id 29
- Requisito 4.4.2.5 - feature_id 30
- Requisito 4.5.1 - feature_id 31
- Requisito 4.5.2 - feature_id 32
- Requisito 4.6.1 - feature_id 33
- Requisito 4.6.2 - feature_id 34
- Requisito 4.6.3 - feature_id 35
- Requisito 5.1 - feature_id 36
- Requisito 5.2 - feature_id 37
- Requisito 5.3 - feature_id 38
- Requisito 5.4 - feature_id 39
