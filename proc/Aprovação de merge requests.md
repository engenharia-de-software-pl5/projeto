# Procedimento para a aprovação de um merge request

**Etapas a seguir para quem deseja realizar um merge:**

1. Push branch local para o remoto;
2. Selecionar *Merge requests* no GitLab;
3. Clicar no botão azul no canto superior direito *New merge request;*
4. Selecionar source branch (aquele que deu push);
5. Selecionar target branch, onde irá dar merge;
6. Clicar botão azul *Compare branches and continue;*
7. Escrever título, descrição e selecionar labels;
8. Selecionar em *Reviewer* gestor da sua equipa;
9. Clicar botão azul *Create merge request.*

**Etapas a seguir pelos gestores de equipa:**

1. Realizar clone do branch em questão;
2. Experimentar código localmente, ou caso seja uma tarefa visual, analisar se está tudo conforme o pretendido;
3. Caso seja o esperado, dar aprove ao merge request;
4. Caso contrário, comentar o que está errado e identificar o autor do merge request.


**Etapas a seguir por quem aceita o merge request:**

1. Verificar se o merge request foi aprovado pelo gestor da equipa do autor;
2. Resolver conflitos localmente caso existam;
3. Caso seja alterações de código, executar a aplicação e ver se funciona;
4. Caso funcione, aceitar o merge request;
5. Caso contrário, notificar o autor do merge do request do sucedido.

**Linha de ações de um merge request:**

Criação do merge request pelo autor -> Análise detalhada pelo gestor da equipa do autor -> Aprovação ou reprovação do merge request pelo gestor da equipa em questão -> Análise rápida por quem aceita o merge request  -> Caso esteja tudo bem, aceitação do merge request.
