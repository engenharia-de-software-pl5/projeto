# Commits

## Preâmbulo

Usar um dos preâmbulos no ficheiro [proc/Conventional-Commits cheat sheet.pdf](https://gitlab.com/engenharia-de-software-pl5/projeto/-/blob/main/proc/Conventional-Commits%20cheat%20sheet.pdf) e escrever uma mensagem representativa do commit.

### Exemplos

- fix: resolvido bug
- chore: adicionado ficheiro com conveção de commits
- test: adicionado testes para requesito 1
- refactor: alteração do código para eliminar redundâncias
