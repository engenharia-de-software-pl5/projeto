# Issues de Defeitos nos mockups de Design


Quando é encontrado um requisito que não está a ser cumprido nos mockups de design, deve ser tratado da seguinte forma:

Criar uma issue com o título: def-des-mock  (issue do requisito)

Corpo da issue:

    - Identificar a imagem ou a vista do mockup em questão

    - Identificar o problema ou os objetos em falta

    - Enviar uma screenshot do erro
    

Assign a issue ao líder de Design.

E determinar quantas horas deverá levar a corrigir o defeito.
