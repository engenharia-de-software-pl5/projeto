# Issue de um Requisito

## Template

**User story <num_user_story>:**  "user_story"

**Requisito:** "requisito"

**Ordem de eventos desejada:** "ordem de eventos"

**Tempo estimado para desenvolvimento:** "tempo_estimado"

## Regex de verificação

([\*]{2,3}User story [0-9.]*:[\*]{2,3}).*\n*
([\*]{2,3}Requisito:[\*]{2,3}).*\n*
([\*]{2,3}Ordem de eventos desejada:[\*]{2,3}).*\n*
([\*]{2,3}Tempo estimado para desenvolvimento:[\*]{2,3}).*\n*

## Linked issues (usar a funcionalidade do gitlab)
