Quando é encontrado um requisito que não está a ser cumprido na backend, deve ser tratado da seguinte forma:

Criar uma issue com o título: def-backend  (issue do requisito)
Corpo da issue:

- Identificar o código em questão
	
- Especificar o problema

- Sugerir uma alteração

	Ex:
	
	Código: dev\src\gitlab_analyzer\name_of_file	linha N	
	Especificação do problema: ...
	Alteração sugerida: ...

Assign a issue ao líder de BackEnd.
E determinar quantas horas deverá levar a corrigir o defeito.
