Quando é encontrado uma issue que não está com a formatação instruída nos procedimentos do tipo da mesma, deve ser tratado da seguinte forma:
Criar uma issue com o título: def-issue-format
Corpo da issue:
Formatação a ser usada: (link da conveção)
Problema: (opcional)

Exemplo do corpo da issue:
Formatação a ser usada: https://gitlab.com/engenharia-de-software-pl5/projeto/-/blob/main/proc/Estrutura%20requisito.md
Problema: Provavelmente falta por entre [] a keyword requisito.

Assign da issue ao criador original da issue.
E determinar quantos minutos deverá levar a corrigir o defeito.
