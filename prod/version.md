# Versões da aplicação

Ficheiro com as várias versões e os requisitos implementados em cada versão

## Versão 1.2 - 4/12/2021

- 4.2
  - 4.2.1
  - 4.2.2
    - 4.2.2.1
    - 4.2.2.2
    - 4.2.2.3
  - 4.2.3
  - 4.2.4
  - 4.2.4
  - 4.2.5

## Versão 1.1 27/11/2021

- 4.1
  - 4.1.1
  - 4.1.3
  - 4.1.5
  - 4.1.6
  - 4.1.7
  - 4.1.8

## Versão 1.0 20/11/2021

- Aplicação base para o ínicio do desenvolvimento
