# Requisitos de produto

<details><summary>4.1. Definições gerais</summary>

- 4.1.1 - O utilizador autenticado deve ter algum campo onde possa inserir o ID do repositório que deseja selecionar e algo para informar o sistema que já introduziu o ID, depois de inserido deve ser levado para a página principal com toda a informação referente a esse repositório. Caso o utilizador não esteja autenticado deverá ser redirecionado para a página de login. Caso o repositório seja privado, deve ser pedido ao utilizador as suas credenciais do gitlab ou o seu token de acesso para conseguir aceder à informação, se o utilizador não tenha permissões, o sistema deve enviar uma mensagem de erro a informar do mesmo. Caso o repositório não exista, o sistema deve apresentar ao utilizador uma mensagem de erro a informar do mesmo.

- 4.1.2 - O utilizador autenticado depois de selecionar o repositório e ter entrado na dashboard, deve estar representado o branch main por default e deve haver alguma forma de mudar o branch para permitir a análise dos outros branches.

- 4.1.3 - Quando o utilizador autenticado entra na dashboard, o sistema deve apresentar uma linha de tempo uniforme ao utilizador.

- 4.1.4 - Os utilizadores, ao chegarem à linha temporal, terão duas opções que quando selecionadas, permitem reduzir ou ampliar a linha temporal.

- 4.1.5 - O utilizador autenticado, ao entrar na dashboard, deverá ter uma representação gráfica de uma timeline em que os eventos da timeline são commits, ou seja, o sistema só mostrará as modificações do repositório entre 2 commits.

- 4.1.6 - O utilizador deve ter uma opção de 'back' em todas as vistas da dashboard, num sitío fixo, permitindo-lhe voltar à página anterior sem ter de realizar todos os passos anteriores necessários para chegar a esta.

- 4.1.7 - A cada novo acesso à dashboard por parte do utilizador autenticado, o sistema recolhe os dados mais recentes do repositório e guarda-os na base de dados para as funcionalidades fazerem uso dos mesmos.

- 4.1.8 - Quando o utilizador autenticado acede a dashboard, página posterior ao login, em todas as vistas deve estar presente, num lugar fixo, o tempo da última atualização dos dados por parte dos sistema.

</details>

<details><summary>4.2. Vista do repositório</summary>

- 4.2.1 - Os utilizadores, ao entrarem no repositório, deverão ter um campo que lhes apresente a árvore dos ficheiros, devendo ser possível dar "expand" ou "colapse" às diferentes diretorias consoante o fichero a que se pertende aceder.

- 4.2.2 - Na dashboard, deve haver uma estrutura gráfica com todos os ficheiros presentes no repositório, organizados por diretorias, que permita clicar numa diretoria e ver os ficheiros nela contida e um método de regresso para a diretoria pai (como no gitlab).

    - 4.2.2.1 - Na representação gráfica dos ficheiros do repositório, deve existir a possibilidade de selecionar a vista por categoria. Deve também ser definido o modo de representar as categorias distintamente, para ser apresentado na representação gráfica dos ficheiros do repositório.

    - 4.2.2.2 - Um utilizador autenticado, quando acede à dashboard e visualiza a repesentação gráfica dos ficheiros presentes no repositório, pode selecionar a vista por churn. O sistema apresentará todos os ficheiros com um gradiente de cor, de modo a distinguir um ficheiro muito alterado de um pouco alterado, o sistema irá utilizar os commits que alteraram esse ficheiro e o número total de commits para calcular o gradiente.

    - 4.2.2.3 - O utilizador autenticado ao aceder à representação gráfica dos ficheiros presentes no repositório, deve ter opção de clicar nos ficheiros, quando um ficheiro é clicado, o sistema deverá apresentar ao utilizador o conteúdo daquele ficheiro no último commit.

- 4.2.3 - Na representação gráfica dos ficheiros no repositório deve existir uma opção que permita ao utilizador visualizar ficheiros agrupados por categoria. O sistema deverá ter algum tipo de estrutura de hierarquia pai-filho em que as categorias são o pai e os ficheiros dessa categoria são os filhos.

- 4.2.4 - O utilizador autenticado deve ter a opção de agrupar os ficheiros por autor na árvore de ficheiros do repositório disponibilizada na dashboard. Se o utilizador autenticado clicar num desses ficheiros o sistema deverá redirecionar o utlizador para a história desse ficheiro (requisito #56). Caso o utilizador clique no autor o sistema deverá redirecionar o utilizador para os ficheiros alterados por esse autor (parte que teré de ser implementada por quem desenvolver este requisito).

- 4.2.5 - Na representação gráfica dos ficheiros no repositório deve existir uma opção que permita ao utilizador visualizar ficheiros agrupados por intensidade de alterações. A intensidade de alterações calcula-se fazendo o quociente do número de commit's em que o ficheiro foi modificado por o número total de commit's do repositório. Deve ser definida uma escala de intensidade numérica, por exemplo (poucas alterações) 1 - 5 (muitas alterações). O sistema deverá ter algum tipo de estrutura de hierarquia pai-filho em que os elementos da escala de intensidade de alterações são os pais e os ficheiros classificados por intensidade de alterações são os filhos.

</details>

<details><summary>4.3. Vista dos ficheiros</summary>

- 4.3.1 - Quando o utilizador clica num ficheiro no modo de agrupamento por categoria ou por intensidade de alterações (requisitos #51 e #55) ou quando o utilizador seleciona opção 'histórico' na representação gráfica dos ficheiros do repositório (como no gitlab), o utilizador deve ser redirecionado para uma vista com a história desse ficheiro. Nessa vista deve estar uma lista de commits que alteraram o ficheiro em questão, bem como o autor do commit e os defeitos pendentes, ou seja, uma lista com todos os issues que estejam associados a defeitos nesse ficheiro. Quando o utilizador clica num commit específico, deve ser redirecionado para uma vista com todas as alterações ao ficheiro apresentadas de forma visual num visualizador de texto (como o gitlab faz).

</details>