# Users Stories

<details><summary>4.1. Definições gerais</summary>

- [x] 4.1.1 Como _utilizador_, **_quero_** escolher o repositório GitLab a analisar **_para_** poder visualizar as informações nele contido.

- [x] 4.1.2 Como _utilizador_, **_quero_** escolher o branch que desejo visualizar **_para_** ver os conteúdos em cada um individualmente.

- [x] 4.1.3: Como _utilizador_ autenticado, após seleção do repositório, bem como do branch, **_quero_** ver a linha de tempo distríbuida uniformemente por eventos temporais no repositório, **_para_** facilitar a visualização de todas as alterações a ficheiros.

- [x] 4.1.4: Como _utilizador_ autenticado, após seleção do repositório, bem como do branch, **_quero_** ampliar, ou reduzir a linha de tempo vísivel **_para_** ter uma melhor perspetiva de uma série de eventos localizada.

- [x] 4.1.5 Como _utilizador_ **_quero_** organizar os eventos por commit instants **_para_** criar uma linha temporal.

- [x] 4.1.6 Como _utilizador_ **_quero_** uma funcionalidade 'back' **_para_** regressar à vista anteriormente apresentada sem ter de reinserir as opções que levaram à sua visualização.

- [x] 4.1.8 Como _utilizador_ **_quero_** saber o momento da última atualização dos dados **_para_** saber o quão atualizada está a informação que estou a ver.
</details>

<details><summary>4.2. Vista do repositório</summary>

- [x] 4.2.1: Como _utilizador_, **_quero_** ver a árvore de ficheiros do repositório, **_para_** ter uma visão clara da estrutura/do esqueleto do projeto.

- [x] 4.2.2.1: Como _utilizador_ ao a árvore de diretorias/ficheiros do repositório, **_quero_** ver os ficheiros presentes em cada pasta, com nome e extensão para ver cada tipo de diretoria deve ser distinta das restantes (REQ, DESIGN, CODE, TEST, CONFIG, etc.. ), para ter uma noção dos ramos por categoria.

- [ ] 4.2.2.2: Como _utilizador_ ao a árvore de diretorias/ficheiros do repositório, **_quero_** a vista dos ficheiros presentes em cada pasta deve ser vista por algum tipo de "churn" **_para_** ver os ficheiros que podem ter mais problemas, por estarem a sofrer atualizações regulares.

- [x] 4.2.2.3: Como _utilizador_ ao a árvore de diretorias/ficheiros do repositório após selecionar um ficheiro, **_quero_** que todo o conteúdo do commit do ficheiro seja visível, **_para_** ter uma visulização rápido do conteúdo.

- [x] 4.2.3 Como _utilizador_ **_quero_** ter acesso a todos os ficheiros do repositório agrupados por categoria **_para_** ter maior facilidade no acesso aos ficheiros de uma determinada categoria.

- [x] 4.2.4 Como _utilizador_, após a seleção de um ficheiro (4.2.3) e selecionado um autor, **_quero_** visualizar a lista de todos os ficheiros do repositório criados/alterados por esse mesmo autor **_para_** ter maior facilidade na perceção da relação do ficheiro em questão com o conjunto de ficheiros posteriormente trablhados pelo autor selecionado.

- [ ] 4.2.5.1 Como _utilizador_ **_quero_** visualizar a lista de todos os ficheiros do repositório agrupados por intensidade de alteração dos mesmos **_para_** perceber se existem ficheiros mortos, ou altamente concorridos.

- [ ] 4.2.5.2 Como _utilizador_ estando na vista (4.2.5.1) e após selecionar um dos ficheiros, **_quero_** visualizar (4.3) **_para_** ter uma ideia da história do ficheiro.

- [ ] 4.2.5.3 Como _utilizador_ estando na vista (4.2.5.1) e selecionado um autor, **_quero_** visualizar a lista de todos os ficheiros do repositório criados/alterados por esse mesmo autor **_para_** ter maior facilidade na perceção da relação do ficheiro em questão com o conjunto de ficheiros posteriormente trablhados pelo autor selecionado.
</details>

<details><summary>4.3. Vista dos ficheiros</summary>

- [ ] 4.3.1.1: Como _utilizador_, **_quero_** ter acesso a uma lista com os commits efetuados sobre um ficheiro selecionado, bem como o autor de cada commit, **_para_** ter uma visão melhor do que se passa, ou passou, com o ficheiro.

- [ ] 4.3.1.2: Como _utilizador_, **_quero_** ver as alterações gerais efetuadas por cada commit, numero de linhas alteradas, acrescentadas e removidas **_para_** ter noção do tamanho de cada alteração.

- [ ] 4.3.1.3: Como _utilizador_, **_quero_** ver referências, tickets/issues, a possiveis defeitos pendentes **_para_** ter noção quantitativa das alterações ainda necessárias.

- [ ] 4.3.2 Como _utilizador_, **_quero_** saber quais os issues associados a cada ficheiro, sendo possível consultar tanto a timeline como o conteúdo destes mesmos issues, **_para_** manter o workflow e a organização do projeto.

- [ ] 4.3.3: Como _utilizador_ **_quero_** aceder a informação estática do ficheiro, i.e: o tipo (REQ/TST...), a dimensão do mesmo, métricas de estabilidade e ainda, caso o ficheiro for código, a sua complexidade **_para_** obter uma ideia geral do ficheiro em questão.
</details>

<details><summary>4.4. Vista Social</summary>
<details><summary>4.4.1. Da equipa</summary>

- [ ] 4.4.1.1 Como _utilizador_ **_quero_** aceder a uma lista de membros ativos do repositório, os membros que fizeram pelo menos 1 commit, **_para_** ver o nivel de atividade de cada membro.

- [ ] 4.4.1.2 Como _utilizador_ **_quero_** visualizar as contribuições dos membros ativos por categoria (REQ, PM, DEV, TST...) **_para_** ter uma vista mais fragmentada do tipo de contribuição categorizado pelo tipo de ficheiro.

- [ ] 4.4.1.3 Como _utilizador_ **_quero_** visualizar o esforço total aplicado ao projeto por categoria (REQ,PM,DEV,TST...) **_para_** conseguir quantificar as horas de trabalho gastas por categoria de ficheiro.

- [ ] 4.4.1.4 Como _utilizador_ **_quero_** visualizar a evolução do esforço aplicado por categoria de ficheiro (REQ, DEV...) **_para_** ter uma ideia do ritmo de trabalho desenvolvido por categoria.
</details>
<details><summary>4.4.2. Por membro</summary>

- [ ] 4.4.2.1 Como _utilizador_ **_quero_** aceder ás paginas de perfil dos membros ativos **_para_** ter uma ideia do trabalho e do membro.

- [ ] 4.4.2.2 Como _utilizador_ **_quero_** aceder á lista de contribuições por categoria **_para_** cada membro ativo **_para_** ter maior facilidade na análise e controlo das contribuições de cada membro do projeto.

- [ ] 4.4.2.3: Como _utilizador_, **_quero_** visualizar o papel de cada membro ativo a partir da intensidade das suas contribuições por categoria, **_para_** saber a equipa a que pertence cada membro.

- [ ] 4.4.2.4: Como _utilizador_, pretendo observar a lista de commits de cada membro ativo **_para_** visualizar todas as suas contribuições.

- [ ] 4.4.2.4: Como _utilizador_, **_quero_** visualizar a timeline dos commits de cada membro ativo **_para_** poder ver os commits feitos no espaço tempo.
</details>
</details>

<details><summary>4.5. Vista de Issues</summary>

- [ ] 4.5.1: Como _utilizador_ **_quero_** ver uma timeline de issues do repositório em cada instante **_para_** ver a evolução das issues do repositório/projeto ao longo do tempo.

- [ ] 4.5.2: Como _utilizador_, após clicar num instante (4.5.1), **_quero_** ver a lista de issues abertos e ativos nesse instante,**_para_** perceber de que forma o trabalho do projeto se organizou, e progrediu naquele instante.
</details>

<details><summary>4.6. Visão Dinâmica</summary>

- [ ] 4.6.1: Como _utilizador_ **_quero_** que cada pagina tenha uma timeline que permita ver o estado do repositório em cada instante,
      **_para_** ver a evolução do repositório/projeto ao longo do tempo.

- [ ] 4.6.2: Como _utilizador_, ao trocar entre instantes na timeline presente na página de visualização, **_quero_** ver a evolução da árvore do repositório **_para_** ter uma melhor perceção do progresso do projeto.

- [ ] 4.6.3: Como utilizador **quero** que para cada commit seja possível ver quais os ficheiros modificados e os respetivos autores, **para** uma maior facilidade na observação da evolução do projeto e para uma melhor organização.
</details>

<details><summary>5. Requisitos não funcionais</summary>

- [ ] 5.1 Como _administrador_, pretendo que apenas utilizadores autenticados possam aceder à informação interna do projeto **_para_** haver segurança e privacidade.

- [ ] 5.2 Como _administrador_, **_quero_** poder apagar informações da dashboard e não do repositório, **_para_** a libertar espaço na base de dados.

- [ ] 5.3 Como _administrador_, **_quero_** que a dashboard tenha uma boa usabilidade, ou seja, navegável entre vistas, intuitiva, responsiva, e eficiente **_para_** facilitar e tornar mais apetecivel o uso da ferramenta.

- [ ] 5.4 Como _administrador_, **_quero_** que a dashboard seja multi-idioma **_para_** atrair utilizadores de diversos idiomas.

- [ ] 5.5 Como _administrador_, **_quero_** que a solução seja web-based **_para_** facilitar ao utilizador o uso da dashboard, **_para_** ser mais prático.
</details>
