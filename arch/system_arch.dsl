    workspace "System_Context" "Description" {

        model {
            anonymous = person "Utilizador Anónimo" "Utilizador não autenticado que acessa à aplicação web"
            auth_user = person "Utilizador Autenticado" "Analisador de Repositórios do Git"
            admin = person "Administrador" "Analisador de Repositórios do Git, com acessos priviligiado aos dados da aplicação"
            git_lab = softwareSystem "GitLab" "Aplicação web que facilita a interação com repositórios de dados do Git." "Sistema Existente"
            aplicacao_web = softwareSystem "Aplicação Web" "" {
                
                site = container "Web Browser" "Apresenta o conteúdo da página fornecida pela Aplicação Web no browser do Utilizador" "HTML, CSS, JS" "Web Browser"
                data_bd = container "Base de Dados dos Repositórios" "Contém os dados sobre o repositório selecionado pelo utilizador, que são representados pela aplicação." "MongoDB" "Data_Base"
                auth_bd = container "Base de Dados de Autenticação" "Contém os dados sobre as autenticações dos utilizadores" "SQLite" "Data_Base"
                
                app = container "Aplicação Web" "Fornece dashboards e ferramentas que facilitam a análise a evolução de projetos contidos em repositórios do Git" "Python, Django" {
                    autenticador = component "Autenticador de Utilizador" "Encripta a palavra passe inserida e verifica os dados de Login"
                    selecionador_repositorio = component "Selecionador de Repositorio" ""
                    gitlab_conector = component "Conector de GitLab" "Comunica com a Aplicação GitLab" "PyDriller, API do Gitlab"
                    data_bd_conector = component "Conector de Dados do Repositório" "Comunica com a base de dados MongoDB" "PyMongo"
                    auth_bd_conector = component "Conector da Base de Dados com os dados de autenticação" "" "SQLite3"
                    dashboard = component "DashBoard" "Apresenta a estrutura gráfica do repositório pelo filtro selecionado" "Python, Django, Html, Css"
                    back = component "Back" "Contêm os dados sobre o histórico de vistas selecionadas" "Python, Stack"
                    filtro = component "Filtro de Visualização" "Contêm as configurações sobre a visualização da vista selecionada" "Python"
                    repo = component "Vista de Repositorio" "Contem os dados necessários para a representação da vista do repositório na dashboard" "Python/JSON" 
                    file = component "Vista de Ficheiro" "Contem os dados necessários para a representação da vista do ficheiro selecionado" "Python/JSON"
                    social = component "Vista Social" "Contem os dados necessários para a representação da vista do ficheiro selecionado" "Python/JSON"
                    issues = component "Vista de Issues" "Contem os dados necessários para a representação da vista de issues do repositório" "Python/JSON"
                    refresh = component "Refresh" "Atualiza os dados do repositório na base de dados" "Python" 
                }

            }

            # --------------------------|
            #                           |
            # Relações de Contexto      | 
            #                           |
            # --------------------------|
            
            # Pessoa - Aplicação
            anonymous -> aplicacao_web "Cria conta ou faz login na sua conta" "HTTPS"
            auth_user -> aplicacao_web "Escolhe repositórios a examinar, do Git. Vê dashboards com a evolução desse repositório ao longo do tempo." "HTTPS"
            admin -> aplicacao_web "Seleciona repositórios do git. Elimina dados internos do Sistema" "HTTPS"
            
            #  Aplicação - Gitlab
            aplicacao_web -> git_lab "Lê dados de" "HTTPS"

            # --------------------------|
            #                           |
            # Relações de Containers    |
            #                           |
            # --------------------------|

            # Utilizador - Aplicação
            anonymous -> site "Cria conta ou faz login na sua conta" ""
            auth_user -> site "Escolhe repositórios a examinar, do git" ""
            admin -> site "Seleciona repositórios do git. Elimina dados internos do Sistema" ""
            
            # Aplicação - Base de Dados
            app -> data_bd "Lê dados de e escreve para" "PyMongo API"
            app -> auth_bd "Lê dados de e escreve para" "SQL" 
            # Aplicação - Gitlab
            app -> git_lab "Faz pedidos a partir de uma API, de modo a receber dados de repositórios do Git" "HTTPS"
            
            # Aplicação - Browser
            app -> site "Fornece o código da página a ser apresentada" "HTTPS"
            site -> app "Faz pedidos API a" "HTTPS"
            
            #---------------------------|
            #                           |
            # Relações de Components    |
            #                           |
            #---------------------------|

            #user fica na bd. verificação de autenticação por aí
            
            #anonymous -> autenticador "Insere dados de Login ou de Sign in" ""
            autenticador -> auth_bd_conector "Envia dados de sign in a" 
            auth_bd_conector -> autenticador "Envia dados de login presentes na base de dados a"
            auth_bd_conector -> auth_bd "Lê dados de e envia dados a"
            
            #auth_user -> filtro "Seleciona os filtros e estruturas de visualização" "Python/Django"
            #auth_user -> selecionador_repositorio "Insere o ID de repositorio a analisar" ""
            
            selecionador_repositorio -> gitlab_conector "Utiliza" ""
            
            #pipeline desde a leitura dos dados à representação na dashboard
            gitlab_conector -> git_lab "Recebe os dados do repositório de e Envia dados a" "HTTPS/JSON"
            gitlab_conector -> data_bd_conector "Fornece os dados a" "Python"
            refresh -> gitlab_conector "Utiliza"
            
            data_bd_conector -> data_bd "Escreve os dados em e lê dados de" "PyMongo Queries"

            repo -> data_bd_conector "Utiliza"
            file -> data_bd_conector "Utiliza"
            social -> data_bd_conector "Utiliza"
            issues -> data_bd_conector "Utiliza"

            
            repo -> dashboard "Fornece os dados a"
            file -> dashboard "Fornece os dados a"
            social -> dashboard "Fornece os dados a"
            issues -> dashboard "Fornece os dados a"
            back -> dashboard "Fornece os dados da ultima vista introduzida na pilha a" "Python, Pop"
            filtro -> dashboard "Fornece as configurações de filtros a" "Python/Django"
            
        }
        views {
            systemContext aplicacao_web {
                include *
                autolayout
            }

            container aplicacao_web {
                include *
                autoLayout
            }

            component app {
                include *
                autoLayout
            }
            theme default
            
            styles {
                element "Sistema Existente" {
                    background #999999
                }
                
                element "Web Browser" {
                    shape WebBrowser
                }

                element "Data_Base" {
                    shape Cylinder
                }
            }
        }
    }
