# Requisito 4.2.1

## Tree View
![](https://i.ibb.co/x8Cz5mh/4-2-1sss.png)

### Nota para o Líder de DES

É importante saber que quando fores dar merge deste requisito no main, é necessário incluires também o ficheiro javascript e o ficheiro css incluidos neste ficheiro .bss

### Nota para o Líder de DEV

É importante tomar atenção ao nome das classes dos objetos da lista.

### Fontes

https://codepen.io/axelaredz/pen/yFbut
